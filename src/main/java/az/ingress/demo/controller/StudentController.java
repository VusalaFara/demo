package az.ingress.demo.controller;

import az.ingress.demo.model.Student;
import az.ingress.demo.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {


    private final StudentService service;

   @GetMapping("/{id}")
    public Student getStudentById(@PathVariable Integer id){
        return service.findById(id).get();
    }

    @GetMapping
    public List<Student> getStudent(){
      return service.getStudent();

    }

    @PostMapping
    public String saveStudent(@RequestBody Student student){
    service.saveStudent(student);
    return "added student with name: "+student.getName();
    }
    @PutMapping ("/{id}")
    public String updateStudent(@PathVariable Integer id,@RequestBody Student student){
    service.updateStudent(id, student);
    return "updated student with id: "+id;
    }

    @DeleteMapping("/{id}")
    public String deleteStudent(@PathVariable Integer id){
        service.deleteStudent(id);
        return "deleted student with id: "+id;
    }
}
