package az.ingress.demo.controller;

import az.ingress.demo.model.Student;
import az.ingress.demo.model.Teacher;
import az.ingress.demo.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/teacher")
@RequiredArgsConstructor
public class TeacherController {

    private final TeacherService service;

    @GetMapping("/{id}")
    public Teacher getTeacherById(@PathVariable Integer id){
        return service.findById(id).get();
    }

    @GetMapping
    public List<Teacher> getTeacher(){
        return service.getTeacher();

    }

    @PostMapping
    public String saveTeacher(@RequestBody Teacher teacher){
        service.saveTeacher(teacher);
        return "added teacher with name: "+teacher.getName();
    }
    @PutMapping ("/{id}")
    public String updateTeacher(@PathVariable Integer id,@RequestBody Teacher teacher){
        service.updateTeacher(id, teacher);
        return "updated teacher with id: "+id;
    }

    @DeleteMapping("/{id}")
    public String deleteTeacher(@PathVariable Integer id){
        service.deleteTeacher(id);
        return "deleted teacher with id: "+id;
    }
}
