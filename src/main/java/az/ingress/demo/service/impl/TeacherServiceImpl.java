package az.ingress.demo.service.impl;

import az.ingress.demo.model.Teacher;
import az.ingress.demo.repository.TeacherRepository;
import az.ingress.demo.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class TeacherServiceImpl implements TeacherService {

    private final TeacherRepository repository;
    @Override
    public Optional<Teacher> findById(Integer id) {
        return repository.findById(id);
    }

    @Override
    public List<Teacher> getTeacher() {
        return repository.findAll();
    }

    @Override
    public String saveTeacher(Teacher teacher) {
        repository.save(teacher);
        return "saved";
    }

    @Override
    public Teacher updateTeacher(Integer id, Teacher teacher) {
        Optional<Teacher> teacher1 = repository.findById(id);
        return repository.save(Teacher.builder()
                .id(teacher1.get().getId())
                .name(teacher.getName())
                .surname(teacher.getSurname())
                .age(teacher.getAge())
                .build());
    }

//    public Teacher updateTeacher(Integer id, Teacher teacher){
//       Optional<Teacher> teacher1 = repository.findById(id);
//       teacher1.get().setName(teacher.getName());
//       teacher1.get().setSurname(teacher.getSurname());
//       teacher1.get().setAge(teacher.getAge());
//       return repository.save(teacher1.get());
//
//   }



//    public Teacher updateTeacher(Integer id, Teacher teacher){
//         return  repository.findById(id).stream().map(teacher1 -> {
//             teacher1.setName(teacher.getName());
//             teacher1.setSurname(teacher.getSurname());
//             teacher1.setAge(teacher.getAge());
//             return repository.save(teacher1);
//         }).findAny().get();
//    }


    @Override
    public Integer deleteTeacher(Integer id) {
        repository.deleteById(id);
        return id;
    }
}
