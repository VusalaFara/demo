package az.ingress.demo.service.impl;

import az.ingress.demo.model.Student;
import az.ingress.demo.repository.StudentRepository;
import az.ingress.demo.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository repository;


    @Override
    public Optional<Student> findById(Integer id) {
        return repository.findById(id);

    }

    @Override
    public List<Student> getStudent() {
        return repository.findAll();
    }

    @Override
    public String saveStudent(Student student) {
        repository.save(student);
        return "added";
    }
     //works

    ////   public String updateStudent(Integer id, Student student){
////       Optional<Student> student1 = repository.findById(id);
////       student1.get().setName(student.getName());
////       student1.get().setSurname(student.getSurname());
////       student1.get().setAge(student.getAge());
////       repository.save(student1.get());
////       return "updated";
////   }
//
//
    //works

////    public Student updateStudent(Integer id, Student student){
////         return  repository.findById(id).stream().map(student1 -> {
// /             student1.setName(student.getName());
////             student1.setSurname(student.getSurname());
////             student1.setAge(student.getAge());
////             return repository.save(student1);
////         }).findAny().get();
////    }

    @Override
    public String updateStudent(Integer id, Student student) {
        Optional<Student> student1 = repository.findById(id);
        repository.save(Student.builder()
                .id(student1.get().getId())
                .name(student.getName())
                .surname(student.getSurname())
                .age(student.getAge())
                .build());
        return "updated";
    };


    @Override
    public Integer deleteStudent(Integer id) {
        repository.deleteById(id);
        return id;
    }
}
