package az.ingress.demo.service;

import az.ingress.demo.model.Teacher;

import java.util.List;
import java.util.Optional;

public interface TeacherService {

    public Optional<Teacher> findById(Integer id);
    public List<Teacher> getTeacher();
    public String saveTeacher(Teacher teacher);
    public Teacher updateTeacher(Integer id, Teacher teacher);
    public Integer deleteTeacher(Integer id);
}
