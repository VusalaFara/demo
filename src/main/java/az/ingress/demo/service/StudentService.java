package az.ingress.demo.service;

import az.ingress.demo.model.Student;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


public interface StudentService {


    public Optional<Student> findById(Integer id);
    public List<Student> getStudent();
    public String saveStudent(Student student);
    public String updateStudent(Integer id, Student student);
    public Integer deleteStudent(Integer id);

    }



